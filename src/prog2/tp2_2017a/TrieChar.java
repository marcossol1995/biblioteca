package prog2.tp2_2017a;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TrieChar<V> {
	private Nodo<V> raiz;
	private static Alfabeto<Character> alf;
	private HashSet<String> claves;

	public TrieChar(Alfabeto<Character> alf) {
		this.alf = alf;
		claves = new HashSet<String>();
	}

	/**
	 * Agrega una cadena a la estructura, asoci�ndole un determinado valor.
	 *
	 * Si la clave ya exist�a, se reemplaza su valor asociado.
	 */
	public void agregar(String clave, V valor) {
		int cont = 0;
		if (this.raiz == null) {
			this.raiz = new Nodo(this.alf.tam());
		}
		Nodo temporal = new Nodo(this.alf.tam());
		temporal = this.raiz;
		for (int i = 0; i < clave.length(); i++) {
			Nodo<V> nuevo = new Nodo(this.alf.tam());
			if (temporal.hijo(this.alf.indice(clave.charAt(i))) != null) {
				temporal = temporal.hijo(this.alf.indice(clave.charAt(i)));
			} else {
				temporal.setHijo(this.alf.indice(clave.charAt(i)), nuevo);
				temporal = temporal.hijo(this.alf.indice(clave.charAt(i)));
				cont++;
			}

		}
		temporal.val = valor;
		claves.add(clave + ":" + valor);
	}

	/**
	 * Devuelve el valor asociado a una clave, o null si no existe.
	 */
	public V obtener(String clave) {
		Nodo temporal = new Nodo(this.alf.tam());
		temporal = this.raiz;
		if (temporal == null) {
			return null;
		}
		for (int i = 0; i < clave.length(); i++) {
			if (temporal.hijo(this.alf.indice(clave.charAt(i))) != null) {
				temporal = temporal.hijo(this.alf.indice(clave.charAt(i)));
			} else {
				return null;
			}
		}
		if (temporal.val == null) {
			return null;
		} else {
			return (V) temporal.val;
		}

	}

	/**
	 * Devuelve una lista con todos los valores cuyas claves empiezan por un
	 * determinado prefijo.
	 */
	public List<V> busqueda(String prefijo) {
		ArrayList retorno = new ArrayList();
		Nodo temporal = new Nodo(this.alf.tam());
		temporal = this.raiz;
		if (temporal == null) {
			return retorno;
		}
		for (int i = 0; i < prefijo.length(); i++) {
			if (temporal.hijo(this.alf.indice(prefijo.charAt(i))) == null) {
				return retorno;
			}
			temporal = temporal.hijo(this.alf.indice(prefijo.charAt(i)));
		}
		if (temporal.val != null) {
			retorno.add(temporal.val);
		}
		return busquedaRec(temporal, retorno);

	}

	private List<V> busquedaRec(Nodo temporal, List<V> retorno) {
		if (temporal == null) {
			return retorno;
		} else {
			for (int i = 0; i < alf.tam(); i++) {
				if (temporal.hijo(i) != null) {
					if (temporal.hijo(i).val != null) {
						retorno.add((V) temporal.hijo(i).val);
					}
					busquedaRec(temporal.hijo(i), retorno);
				}

			}
		}
		return retorno;
	}

	public boolean equals(Object otro) {
		if (otro instanceof TrieChar) {
			TrieChar aux = (TrieChar) otro;
			if (!(this.alf.tam() == aux.alf.tam())) {
				return false;
			} else {
				Queue<Nodo> cola = new LinkedList<Nodo>();
				Queue<Nodo> colaFinal = new LinkedList<Nodo>();
				colaFinal = recorridoSimple(this.raiz, cola);
				Queue<Nodo> colaOtro = new LinkedList<Nodo>();
				Queue<Nodo> colaOtroFinal = new LinkedList<Nodo>();
				colaOtroFinal = recorridoSimple(aux.raiz, colaOtro);
				while (!cola.isEmpty()) {
					Nodo primero = colaFinal.poll();
					Nodo segundo = colaOtroFinal.poll();
					if (primero == null && segundo != null) {
						return false;
					}
					if (primero != null && segundo == null) {
						return false;
					}
					if (primero != null && segundo != null) {
						if (!(primero.val == segundo.val)) {
							return false;
						}
					}
				}
			}

		}
		return true;
	}

	private Queue<Nodo> recorridoSimple(Nodo temporal, Queue<Nodo> cola) {
		if (temporal == null) {
			return cola;
		}
		for (int i = 0; i < this.alf.tam(); i++) {
			if (temporal.hijo(i) != null) {
				cola.add(temporal.hijo(i));
				recorridoSimple(temporal.hijo(i), cola);
			} else {
				cola.add(temporal.hijo(i));
			}
		}
		return cola;

	}

	public String toString() {
		return claves.toString();
	}
}
