package prog2.tp2_2017a;

public class AlfabetoADN implements Alfabeto<Character> {
	@Override
	public int tam() {
		return 4;
	}

	public int indice(Character c) {
		if (c == 'A')
			return 0;
		if (c == 'C')
			return 1;
		if (c == 'G')
			return 2;
		if (c == 'T')
			return 3;

		throw new RuntimeException("digito no v�lido: " + c);
	}

}
