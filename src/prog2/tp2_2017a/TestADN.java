package prog2.tp2_2017a;

import java.util.LinkedList;
import java.util.Queue;

public class TestADN {

	public static void main(String[] args) {
		TrieChar<String> tiposDeAdn;
		tiposDeAdn = new TrieChar<>(new AlfabetoADN());

		// Inventamos algunas cosas para agregarle.
		tiposDeAdn.agregar("ACGT", "vaca");
		tiposDeAdn.agregar("AC", "humano");
		tiposDeAdn.agregar("TA", "alienigena");
		tiposDeAdn.agregar("CG", "perro");

		// Obtenemos cada uno de los significados anteriormente agregados y los
		// imprimimos.
//		System.out.println(tiposDeAdn.obtener("ACGT"));
//		System.out.println(tiposDeAdn.obtener("AC"));
//		System.out.println(tiposDeAdn.obtener("TA"));
//		System.out.println(tiposDeAdn.obtener("CG"));

		// Consultamos por prefijos y mostramos si es que hay alguno.
//		System.out.println(tiposDeAdn.busqueda("A"));
//		System.out.println(tiposDeAdn.busqueda("T"));
//		System.out.println(tiposDeAdn.busqueda("G"));
//		System.out.println(tiposDeAdn.busqueda("C"));

		// Probamos la igualdad entre trie's, creamos un par y comprobamos
		TrieChar<String> tiposDeAdn2;
		tiposDeAdn2 = new TrieChar<>(new AlfabetoADN());
		TrieChar<String> tiposDeAdn3;
		tiposDeAdn3 = new TrieChar<>(new AlfabetoADN());
		TrieChar<String> tiposDeAdn4;
		tiposDeAdn4 = new TrieChar<>(new AlfabetoADN());
		// tiposDeAdn3 no tiene nada agregado, definitivamente no son iguales
		System.out.println(tiposDeAdn.equals(tiposDeAdn3));
		//Agregamos algunas cosas a tiposDeAdn2
		tiposDeAdn2.agregar("AC", "vaca");
		tiposDeAdn2.agregar("ACAAAG", "gato");
		tiposDeAdn2.agregar("TATT", "pez");
		tiposDeAdn2.agregar("CGAT", "elefante");
		// Las claves y significados de tiposDeAdn2 no se corresponden con tiposDeAdn
		System.out.println(tiposDeAdn.equals(tiposDeAdn2));
		//Preparamos tiposDeAdn4
		tiposDeAdn4.agregar("ACGT", "vaca");
		tiposDeAdn4.agregar("AC", "humano");
		tiposDeAdn4.agregar("TA", "alienigena");
		tiposDeAdn4.agregar("CG", "perro");
		// Ahora comparo uno de los arboles con el mismo y con tiposDeAdn4 que es igual
		System.out.println(tiposDeAdn.equals(tiposDeAdn));
		System.out.println(tiposDeAdn.equals(tiposDeAdn4));

		// Mostramos el arbol completo (clave : significado)
		System.out.println(tiposDeAdn.toString());
		
		
		
		
	}

}
